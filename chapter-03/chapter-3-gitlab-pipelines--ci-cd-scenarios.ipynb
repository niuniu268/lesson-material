{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "eaece9a4",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Continuous Software Delivery"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7c28ae48",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Before we begin\n",
    "\n",
    "## Mic off\n",
    "## Camera on\n",
    "## Attendance"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23defef0",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Last week's exercise\n",
    "\n",
    "Anybody wants to show us what they did?\n",
    "\n",
    "Let's discuss..."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "49f7714f",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "This week we'll delve deeper in the GitLab CI/CD platform.\n",
    "\n",
    "We'll also look at some more realistic CI/CD pipeline scenarios."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "651d1021",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# GitLab Pipelines\n",
    "\n",
    "A quick recap of what we talked about last time:\n",
    "\n",
    "- **Pipeline**: a collection of **jobs** that are run in a particular order\n",
    "- `.gitlab-ci.yml`: the file that defines our pipeline\n",
    "- **jobs**: main component of a pipeline\n",
    "- **script**: mandatory key in the yaml file, it is what the runner executes\n",
    "- **artifact**: defines an artifact that is created and saved\n",
    "- **stages**: used to define the order of the jobs\n",
    "- **variables**: defines variables for the pipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af8814f7",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Pipeline architecture\n",
    "\n",
    "There are 3 different types of architecture a pipeline can define:\n",
    "\n",
    "- **Basic**: Good for straightforward projects where all the configuration is in one easy to find place.\n",
    "- **DAG** (Directed Acyclic Graph): Good for large, complex projects that need efficient execution.\n",
    "- **Child/Parent Pipelines**: Good for monorepos and projects with lots of independently defined components."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c586b45",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Basic pipelines\n",
    "\n",
    "These are the default.\n",
    "\n",
    "You have a collection of jobs in different stages.\n",
    "\n",
    "One stage completes before the next starts.\n",
    "\n",
    "When the pipeline gets very large, it can be hard to get an overview."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9bfc6900",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Basic pipelines\n",
    "\n",
    "![](./basic_pipeline.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e3d1afd",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## DAG pipelines\n",
    "\n",
    "When we use the keyword `needs` in our pipeline file we turn it into a DAG pipeline.\n",
    "\n",
    "Jobs in later stages can start even if the previous stage is not completed.\n",
    "\n",
    "Enables faster pipelines.\n",
    "\n",
    "![](./dag_pipeline.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "02c0a194",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Parent-child pipelines\n",
    "\n",
    "Used to divide one complex pipeline into several.\n",
    "\n",
    "Enables simultaneous start of multiple child pipelines.\n",
    "\n",
    "Keywords:\n",
    "\n",
    "- `trigger`, `include`\n",
    "- `rules`, `changes`\n",
    "\n",
    "![](./parent_child_pipeline.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81dcb40a",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Specific branches\n",
    "\n",
    "Different branches in our repo will have different needs: a dev branch may check code formatting and run unit tests, while a staging branch may add to that the integration tests. Finally, our `main` or production branch will need to build and maybe even deploy our application.\n",
    "\n",
    "We can run different sets of jobs depending on branch.\n",
    "\n",
    "We use the `rules` keyword for that.\n",
    "\n",
    "More info and examples [here](https://docs.gitlab.com/ee/ci/yaml/#rules)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f790e4bf",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Pipeline performance\n",
    "\n",
    "Pipeline performance is affected by:\n",
    "\n",
    "- Pipeline complexity\n",
    "    - number of jobs\n",
    "    - number of stages\n",
    "    - Time for the most time-consuming path through the pipeline\n",
    "- Container we run the pipeline in:\n",
    "    - Image size\n",
    "    - Installation of needed software prior to the build at every run of the pipeline\n",
    "    - Create dependencies even when they have not changed\n",
    "- Failing pipelines and re-runs\n",
    "    - Avoid unnecessary terminations of pipelines due to poorly setup tests and quality controls\n",
    "    \n",
    "gitlab.com gives us access to a limited amount of minutes on the free runners, otherwise we'll pay for them. So the quicker our pipelines, the better. That is true even for self-hosted runner (quicker pipelines means one runner can handle more pipelines, and we avoid the need to deploy more runners)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee642dbf",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Artifacts\n",
    "\n",
    "Like an artifact found in a ruin of an ancient temple, `artifacts` in our CI/CD pipelines are things that are left behind after the pipeline is completed.\n",
    "\n",
    "Each run of a pipeline happens in a \"new\" environment, independent of previous runs.\n",
    "\n",
    "Keywords:\n",
    "\n",
    "- `paths`: saves results from the run\n",
    "- `expire_in`:\n",
    "    - Used to automatically delete artifact after a defined time\n",
    "    - Example: `expire_in: 1 week`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3c4d5e6c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Running pipelines\n",
    "\n",
    "How do we run our pipelines?\n",
    "\n",
    "- Manual start\n",
    "- Commit triggered (both regular commits and merge requests)\n",
    "- Scheduled\n",
    "\n",
    "Beware of scheduled runs: it's easy to forget them, and they will eat through your CI/CD minutes if left unchecked! Best avoided if possible."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82612248",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Build time and storage\n",
    "\n",
    "Build time and storage are limited resources gitlab.com gives us.\n",
    "\n",
    "- Build time: 400 free minutes each month\n",
    "    - optimize pipelines for short runs\n",
    "    - avoid scheduled runs\n",
    "    \n",
    "- Data storage\n",
    "    - Avoid uploading large files in repositories if they are not needed to be version controlled\n",
    "    - Use `expires_in` for automated removal of artifacts\n",
    "    - Manually inspect what is saved and where"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "009be65d",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# CI/CD scenarios\n",
    "\n",
    "Let's look at some realistic scenarios for CI/CD pipelines"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1241cb5d",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Code quality\n",
    "\n",
    "CI/CD can automate code quality control, such as linting, checking for unused imports, formatting our code according to a certain standard, ...\n",
    "\n",
    "The tools used vary depending on the language.\n",
    "\n",
    "In python, we can use tools such as:\n",
    "\n",
    "- autopep8\n",
    "- pylint\n",
    "- flake8"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2d7ce892",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Testing\n",
    "\n",
    "We can run our tests automatically as part of the pipeline.\n",
    "\n",
    "In python, we can use the standard library (`unittest`) or other 3rd party tools such as `pytest`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7554e6b8",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Building and packaging\n",
    "\n",
    "We can build and package our application as part of our pipeline.\n",
    "\n",
    "Depending on the language used, this process will vary: compiled languages need to be compiled, while interpreted languages usually package the source code to be interpreted in different ways.\n",
    "\n",
    "We can even build a docker image of our application, and push it to a docker registry."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d4ed4263",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## It's not always about software\n",
    "\n",
    "The above examples assume our repo contains a software project.\n",
    "\n",
    "An example that isn't strictly source code-related: the repository where these lessons' slides and material live have a CI/CD pipeline in place to automatically export the lesson material (jupyter notebook) into html slides, html static content and markdown."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb45fe30",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Scenario\n",
    "\n",
    "Let's say we have a backend API written in Python.\n",
    "\n",
    "Our stages and jobs might look something like:\n",
    "\n",
    "- code_quality\n",
    "    - check for PEP8 compliance. If not compliant, re-format the source files and commit to the same branch, also abort this run (the new commit will trigger a new run of the pipeline)\n",
    "    - linting. Check if there are any antipatterns in our code. If there are, abort the pipeline. The dev will fix the issues and re-commit.\n",
    "- testing\n",
    "    - run unit test, and possibly generate a report of the results (to be saved as an artifact). If not all the test pass, abort the pipeline\n",
    "- building\n",
    "    - if the pipeline comes this far, we can assume the code is PEP8 compliant and linted, as well as tested successfully. We can procees to package it in a docker image and push it to a registry.\n",
    "    \n",
    "**Which architecture do you think is more suitable for the above scenario?**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "376cd752",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## That's all, folks\n",
    "\n",
    "In the next lesson we'll implement the previous scenario together.\n",
    "\n",
    "In the meanwhile, feel free to check out the [official docs](https://docs.gitlab.com/ee/ci/) for GitLab CI/CD."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91ee11f1",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Questions?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6fea2973",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Continuous Software Delivery -- a practical example"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4702676b",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Before we begin\n",
    "\n",
    "## Mic off\n",
    "## Camera on\n",
    "## Attendance"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0031220d",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Today we'll build a working pipeline together.\n",
    "\n",
    "I'll provide the code.\n",
    "\n",
    "As usual, **interrupt me if you have questions, got an error or got stuck**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b0dacdfd",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Intro - YAML\n",
    "\n",
    "We'll write a YAML file called `.gitlab-ci.yml`.\n",
    "\n",
    "Before we get going, I'll introduce YAML live for you in my editor.\n",
    "\n",
    "**LIVE YAML DEMO**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60173a72",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Let's-a-go!\n",
    "\n",
    "Let's build a pipeline together.\n",
    "\n",
    "**SHARE CODE (remove pipeline file first)**\n",
    "\n",
    "**SETUP REPO**\n",
    "\n",
    "**LIVE CODEALONG**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "669f3071",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Question\n",
    "\n",
    "Our pipeline now works, but it has some drawbacks:\n",
    "\n",
    "- the `before_script` is repeated accross jobs\n",
    "- the `before_script` slows down each job\n",
    "\n",
    "How would you fix this?\n",
    "\n",
    "### Exercise\n",
    "\n",
    "For next time, try to figure out a solution to the problems above.\n",
    "\n",
    "We'll discuss your solution next week."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a7b0531",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Questions?"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
