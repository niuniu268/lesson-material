This is a simple example of last lesson's scenario for CI/CD.

We have some trivial code and tests in place.

Together we'll write a pipeline to:
- test code quality
    - check PEP8 compliancy
    - lint the code
- test the code
    - run unittests with pytest, generating a report as an artifact
- build the code
    - create a docker image for our code and push it to a docker registry using 2 different tags:
        - `latest`
        - the commit unique ID
