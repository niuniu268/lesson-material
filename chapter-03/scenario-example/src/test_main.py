'''Some trivial tests.
'''

from main import print

def test_print(capsys):
    print("Hello, world!")
    out, _ = capsys.readouterr()
    assert out == "Hello, world!"