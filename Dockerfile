# This Dockerfile is used to create an image pushed to the gitlab container
# registry. This image is used in the CI/CD pipeline to avoid having to install
# all dependencies as part of the pipeline.
#
# OBS! If you add a new dependency to requirements.txt, you'll need to build the
# image and push it to the registry again.
FROM python:3
RUN pip install --upgrade pip
WORKDIR "/exporter"
COPY requirements.txt .
RUN pip install -r requirements.txt