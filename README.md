[![pipeline status](https://gitlab.com/edugrade2/continuous-software-delivery/lesson-material/badges/master/pipeline.svg)](https://gitlab.com/edugrade2/continuous-software-delivery/lesson-material/-/commits/master)

# Edugrade - Kontinuerlig Mjukvaroleverans / Continuous Software Delivery Course Material

This repo contains all the material for the above course given at Edugrade HT22.
